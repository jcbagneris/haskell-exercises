module Chapter_04 where


-- Awesome exercises

awesome = ["Papuchon", "curry", ":)"]
alsoAwesome = ["Quake", "The Simons"]
allAwesome = [awesome, alsoAwesome]

-- length type definition
-- length takes a list and evaluates to an Int
-- length :: [a] -> Int
-- Note that the real type is more general as it can take
-- any Foldable, not only a list

aw_2a = length [1, 2, 3, 4, 5] == 5
aw_2b = length [(1, 2), (2, 3), (3, 4)] == 3
aw_2c = length allAwesome == 2
aw_2d = length (concat allAwesome) == 5

-- 6 / length [1, 2, 3] yields and error
-- because (/) expects Frational args and length yields an Int

aw_4 = 6 `div` length [1, 2, 3] == 2

-- 2 + 3 == 5 is a Bool, the result is True
aw_5 = (2 + 3 == 5) == True

-- x + 3 == 5 is a Bool, the result is False
aw_6 = (x + 3 == 5) == False
    where x = 5

aw_7a = (length allAwesome == 2) == (True :: Bool)
-- length [1, 'a', 3, 'b'] error as lists constituents must be of the same type
aw_7c = (length allAwesome + length awesome) == (5 :: Int)
aw_7d = ((8 == 8) && ('b' < 'a')) == (False :: Bool)
-- (8 == 8) && 9 error as (&&) expects Bool args and 9 is numeric

isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == reverse xs

myAbs :: Integer -> Integer
myAbs x = if x > 0 then x else (-x)

f :: (a, b) -> (c, d) -> ((b, d), (a, c))
f x y = ((snd x, snd y), (fst x, fst y))

-- Correcting syntax

x = (+)
g xs = w `x` 1
    where w = length xs

cs_1 = g awesome == 4

id' :: a -> a
id' = (\x -> x)
cs_2 = id' alsoAwesome == ["Quake", "The Simons"]

cs_3 = (\(x:xs) -> x) [1, 2, 3] == 1

h (a, b) = a
cs_4 = h (1, 2) == 1

-- Match function names with their types
-- show :: Show a => a -> String
-- (==) :: Eq a => a -> a -> Bool
-- fst :: (a, b) -> a
-- (+) :: Num a => a -> a -> a



