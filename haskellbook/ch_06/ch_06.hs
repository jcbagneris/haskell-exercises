module Chapter_06 where

import Data.List (sort)

-- Eq Instances p.179

data TisAnInteger = TisAn Integer

instance Eq TisAnInteger where
    (==) (TisAn x) (TisAn y) = x == y

ei_1 = TisAn 4 == TisAn 3

data TwoIntegers = Two Integer Integer

instance Eq TwoIntegers where
    (==) (Two x y) (Two x' y') = (x == x') && (y == y')

ei_2 = Two 2 3 == Two 3 2

data StringOrInt = TisAnInt Int | TisAString String

instance Eq StringOrInt where
    (==) (TisAnInt x) (TisAnInt y) = x == y
    (==) (TisAString s) (TisAString t) = s == t
    (==)  _ _ = False

ei_3 = TisAnInt 4 == TisAString "Blah"

data Pair a = Pair a a

instance Eq a => Eq (Pair a) where
    (==) (Pair x y) (Pair x' y') = (x == x') && (y == y')

ei_4 = Pair 5 6 == Pair 5 7

data Tuple a b = Tuple a b

instance (Eq a, Eq b) => Eq (Tuple a b) where
    (==) (Tuple x y) (Tuple x' y') = (x == x') && (y == y')

ei_5 = Tuple 5 'w' == Tuple 5 'z'

data Which a = ThisOne a | ThatOne a

instance Eq a => Eq (Which a) where
    (==) (ThisOne x) (ThisOne y) = x == y
    (==) (ThatOne x) (ThatOne y) = x == y
    (==) _ _ = False

ei_6 = ThisOne 3 == ThatOne 3

data EitherOr a b = Hello a | Goodbye b

instance (Eq a, Eq b) => Eq (EitherOr a b) where
    (==) (Hello x) (Hello y) = x == y
    (==) (Goodbye x) (Goodbye y) = x == y
    (==) _ _ = False

ei_7 = Hello 3 == Goodbye "John"


-- Will they work p.193

wtw_1 = max (length [1, 2, 3]) (length [8, 9, 10, 11, 12]) == 5

wtw_2 = compare (3 * 4) (3 * 5) == LT

-- compare "Julie" True will not work, [Char] and Bool cannot compare

wtw_4 = (5 + 3) > (3 + 6)

-- Chapter exercises

-- Multiple choice
-- 1. The Eq class makes equality tests possible
-- 2. The typeclass Ord is a subclass of Eq
-- 3. The type of > is Ord a => a -> a -> Bool
-- 4. In x = divMod 16 12 the type of x is a tuple
-- 5. The typeclass Integral includes Int and Integer numbers

-- Does it typecheck?

data Person = Person Bool deriving Show
-- we derive show so that the printPerson function typechecks

printPerson :: Person -> IO ()
printPerson person = putStrLn (show person)

data Mood = Blah
          | Woot
          deriving (Show, Eq)
-- here we have to derive Eq for the settleDown function to typecheck
-- because of the x == Woot expression

settleDown x = if x == Woot then Blah else x

-- The acceptable inputs to settleDown are any Mood value, that is Blah or Woot
-- Running settleDown 9 will not typecheck as 9 is not a Mood
-- Blah > Woot will neither typecheck as Mood has no instance for Ord

type Subject = String
type Verb = String
type Object = String

data Sentence = Sentence Subject Verb Object
     deriving (Eq, Show)

s1 = Sentence "dogs" "drool"
-- s1 will not typecheck as there are not enough arguments,
-- Object is missing

s2 = Sentence "Julie" "loves" "dogs"

data Rocks = Rocks String deriving (Eq, Show)
data Yeah = Yeah Bool deriving (Eq, Show)
data Papu = Papu Rocks Yeah deriving (Eq, Show)

-- phew = Papu "chases" True
-- this does not typecheck as Papu expects a Rock and a Yeah,
-- not a String and a Bool
-- it should be
phew = Papu (Rocks "chases") (Yeah True)

-- this one is ok
truth = Papu (Rocks "chomskydoz") (Yeah True)

-- the function typechecks as Papu derives Eq
equalityForall :: Papu -> Papu -> Bool
equalityForall p p' = p == p'

-- comparePapus will not typecheck as Papu does not derive Ord
-- note that if it had to, then Rocks and Yeah should derive Ord
-- as well.

-- Match the types

-- Q1 the typeclass Num is needed to assign 1 to mtm_i
mtm_i :: Num a => a
mtm_i = 1

-- Q2 and Q3 Num is not enough, we need at least Fractional as a typeclass
-- thus Fractional works for Q3
mtm_2_f :: Fractional a => a
mtm_2_f = 1.0

-- Q4 works for RealFrac, which is both Real and Fractional
mtm_4_f :: RealFrac a => a
mtm_4_f = 1.0

-- Q5 Restricting the typeclass to Ord is possible
freud :: Ord a => a -> a
freud x = x

-- Q6 Restricting to a given type is possible too
freud' :: Int -> Int
freud' x = x

-- Q7 It is not possible to generalize to any type the result of sigmund
-- as the assignment to myX forces and Int
-- Q8 Again, the Num typeclass is not enough as we explicitely return an Int
myX = 1 :: Int

sigmund :: a -> Int
sigmund x = myX

-- Q9 Here restricting the list elements to Int is not a problem
jung :: [Int] -> Int
jung xs = head (sort xs)

-- Q10 The Ord typeclass is enough for the list to be sortable
young :: Ord a => [a] -> a
young xs = head (sort xs)

-- Q11 Here as mySort expects [Char] we cannot generalize signifier
-- to accept any Ord a => [a]
mySort :: [Char] -> [Char]
mySort = sort

signifier :: [Char] -> Char
signifier xs = head (mySort xs)

-- Type-Kwon-Do Two

chk :: Eq b => (a -> b) -> a -> b -> Bool
chk f x y = f x == y
-- example: chk head ['a', 'c'] 'c'
-- False

arith :: Num b => (a -> b) -> Integer -> a -> b
arith f i x = f x + fromInteger i
-- example: arith head 2 [3.0, 4.0]
-- 5.0






