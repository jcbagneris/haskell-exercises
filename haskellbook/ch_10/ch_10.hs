module Chapter_10 where

-- Foldl example p.360

-- foldl (flip const) 0 [1..5]
-- f = (flip const)
-- (((((0 `f` 1) `f` 2) `f` 3) `f` 4) `f` 5)
-- (((((const 1 0) `f` 2) `f` 3) `f` 4) `f` 5)
-- ((((1 `f` 2) `f` 3) `f` 4) `f` 5)
-- ((((const 2 1) `f` 3) `f` 4) `f` 5)
-- (((2 `f` 3) `f` 4) `f` 5)
-- (((const 3 2) `f` 4) `f` 5)
-- ((3 `f` 4) `f` 5)
-- ((const 4 3) `f` 5)
-- (4 `f` 5)
-- (const 5 4)
-- 5

-- Understanding folds p.360

uf_1 = foldr (*) 1 [1..5] == foldl (*) 1 [1..5] -- True

-- 2. Write out the evaluation steps for
-- foldl (flip (*)) 1 [1..3]
-- f = flip (:)
-- (((1 `f` 1) `f` 2) `f` 3)
-- ((( 1 * 1) `f` 2) `f` 3)
-- ((2 * ( 1 * 1)) `f` 3)
-- (3 * (2 * (1 * 1)))

-- 3. One difference between foldr and foldl is:
-- foldr, but not foldl, associates to the right

-- 4. Folds are catamorphisms, which means they are generally used to
-- reduce structure

uf5_a = foldr (++) "" ["woot", "WOOT", "woot"] -- "wootWOOTwoot"
uf5_b = foldr max ' ' "fear is the little death" -- 't'
uf5_c = foldr (&&) True [False, True] -- False
uf5_d = foldr (||) True [False, True] -- True, whatever the list
uf5_e = foldr ((++) . show) "" [1..5] -- "12345"
-- Why not foldl? Evaluation steps:
-- foldl ((++) . show) "" [1..5]
-- f = ((++) . show)
-- f x y = y ++ show x
-- ((((("" `f` 1) `f` 2) `f` 3) `f` 4) `f` 5)
-- (((((f "" 1) `f` 2) `f` 3) `f` 4) `f` 5)
-- f "" 1 is an error as f x y = y ++ show x
-- Then of course:
uf5_e' = foldl (flip ((++) . show)) "" [1..5] -- "54321" :)
uf5_f = foldl const 'a' [1..5] -- 'a'
-- Why not foldr ?
-- foldr :: (a -> b -> b) -> b -> [a] -> b
-- const :: a -> b -> a
-- Thus if const is the function used for folder, we should flip its
-- arguments unless they are of the same type.
-- In other words in foldr const 'a' [1..5]
-- the type of b is Char and the type of a is Num, the foldr evaluation
-- is expected to return a Char. It does not as const :: a -> b -> a
-- Then of course:
uf5_f' = foldr (flip const) 'a' [1..5] -- 'a'
-- Same problems as above with the use of const:
uf5_g = foldr (flip const) 0 "tacos" -- 0
uf5_g' = foldl const 0 "tacos" -- 0
uf5_h = foldl const 0 "burritos" -- 0
uf5_h' = foldr (flip const) 0 "burritos" -- 0
uf5_i = foldl const 'z' [1..5] -- 'z'
uf5_i' = foldr (flip const) 'z' [1..5] -- 'z'

-- Scans Exercises

fibs = 1 : scanl (+) 1 fibs
fibsN x = fibs !! x

fibs' = take 20 fibs

fibs'' = filter (<100) fibs'

factorial x = take (x+1) $ scanl (*) 1 [1..]

-- Chapter Exercises
-- Warm-up and review

stops = "pbtdkg"
vowels = "aeiou"

wur_1a = [(x,y,z) | x <- stops, y <- vowels, z <- stops]
wur_1b = filter (\(x,y,z) -> x == 'p') wur_1a
wur_1b' = [('p',x,y) | x <- vowels, y <- stops]

verbs = ["loves","dates","calls"]
nouns = ["Xavier","Yelena","Zoe"]
wur_1c = [(x, y, z) | x <- nouns, y <- verbs, z <- nouns]
-- more realistic
wur_1c' = [(x, y, z) | x <- nouns, y <- verbs, z <- nouns, z /= x]

seekritFunc :: String -> Int
seekritFunc x =
    div (sum (map length (words x))) (length (words x))

seekritFunc' :: String -> Double
seekritFunc' x =
    fromIntegral (sum (map length (words x))) / fromIntegral (length (words x))

-- Rewriting functions using folds

myOr :: [Bool] -> Bool
myOr = foldr (||) False

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr ((||) . f) False

myElem :: Eq a => a -> [a] -> Bool
myElem x = myAny (== x)

myElem' :: Eq a => a -> [a] -> Bool
myElem' x = foldr ((||).(== x)) False

myReverse :: [a] -> [a]
myReverse = foldl (flip (:)) []

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr ((:) . f) []

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f [] = []
myFilter f (x:xs) = if (f x) then x:(myFilter f xs) else myFilter f xs 

myFilter' :: (a -> Bool) -> [a] -> [a]
myFilter' f = foldr (\x xs -> if (f x) then x:xs else xs) []

squish :: [[a]] -> [a]
squish = foldr (++) []

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f = foldr ((++) . f) []

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy f (x:xs) = foldl (\a b -> if f a b == GT then a else b) x (x:xs)

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f (x:xs) = foldl (\a b -> if f a b == LT then a else b) x (x:xs)




