-- End of chapter 3 exercise 6 of "Building functions"
module Reverse where

-- Play with drop and take, more than unsafe.
rvrs :: String -> String
rvrs xs = drop 9 xs ++ take 4 (drop 5 xs) ++ take 5 xs

main :: IO ()
main = print $ rvrs "Curry is awesome"

