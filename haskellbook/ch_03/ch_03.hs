module Chapter_03 where

-- Reading syntax

rs_1a = concat [[1,2,3],[4,5,6]] == [1,2,3,4,5,6]

rs_1b = (++) [1,2,3] [4,5,6] == [1,2,3,4,5,6]

rs_1c = (++) "hello" " world" == "hello world"

rs_1d = "hello" ++ " world" == "hello world"

rs_1e = "hello" !! 4 == 'o'

rs_1f = (!!) "hello" 4 == 'o'

rs_1g = take 4 "lovely" == "love"

rs_1h = take 3 "awesome" == "awe"

rs_2a = concat [[1 * 6], [2 * 6], [3 * 6]] == [6,12,18]

rs_2b = "rain" ++ drop 2 "elbow" == "rainbow"

rs_2c = 10 * head [1, 2, 3] == 10

rs_2d = (take 3 "Julie") ++ (tail "yes") == "Jules"

rs_2e = concat [tail [1, 2, 3],
                tail [4, 5, 6],
                tail [7, 8, 9]]
        == [2, 3, 5, 6, 8, 9]

-- Building functions

bf_1a = "Curry is awesome" ++ "!"

bf_1b = ["Curry is awesome" !! 4]

bf_1c = drop 9 "Curry is awesome!"

bf_2a :: String -> String
bf_2a xs = xs ++ "!"

-- Note that we do not test the length of the strings in the next 2 functions
-- They are unsafe
bf_2b :: String -> String
bf_2b xs = [xs !! 4]

bf_2c :: String -> String
bf_2c xs = drop 9 xs

-- Unsafe too
thirdLetter :: String -> Char
thirdLetter xs = xs !! 2

bf_3 = thirdLetter "Curry is awesome" == 'r'

-- Still unsafe
letterIndex :: Int -> Char
letterIndex n = "Curry is awesome" !! n

bf_4 = letterIndex 2 == 'r'

-- Play with drop and take, more than unsafe.
rvrs :: String -> String
rvrs xs = drop 9 xs ++ take 4 (drop 5 xs) ++ take 5 xs

bf_5 = rvrs "Curry is awesome" == "awesome is Curry"

