{-# LANGUAGE NoMonomorphismRestriction #-}

module Chapter_05 where

-- Multiple choice

-- A value of type [a] is a list whose elements are all of some type a
-- A function of type [[a]] -> [a] could take a list of strings as an argument
-- A function of type [a] -> Int -> a returns one element of type a from a list
-- A function of type (a, b) -> a takes a tuple as argument and returns the first value

-- Determine the type

dt_1a :: Num a => a
dt_1a = (* 9) 6

dt_1b :: Num a => (a, [Char])
dt_1b = head [(0, "doge"), (1, "kitteh")]

dt_1c :: (Integer, [Char])
dt_1c = head [(0 :: Integer, "doge"), (1, "kitteh")]

dt_1d :: Bool
dt_1d = if False then True else False

dt_1e :: Int
dt_1e = length [1, 2, 3, 4, 5]

dt_1f :: Bool
dt_1f = (length [1, 2, 3, 4, 5]) > (length "TACOTAC")

x = 5
y = x + 5
w = y * 10
-- w is of type Num a => a

z y = y * 10
-- z is of type Num a => a -> a

f = 4 / y
-- f is of type Fractional a => a

x' = "Julie"
y' = " <3 "
z' = "Haskell"
f' = x' ++ y' ++ z'
-- f' is of type [Char]

-- Does it compile?

bigNum = (^) 5 $ 10 -- compiles ok, (^) 5 yields a partially applied function 5^, which is then applied to 10 and yields 5^10
-- wahoo = bigNum $ 10 does not compile as bignum is not a function

x'' = print -- compiles
y'' = print "woohoo!" -- compiles
z'' = x'' "hello world!" -- compiles

a = (+)
b = 5
-- c = b 10 does not compile, b is not a function
-- d = c 200 does not compile either

-- a' = 12 + b' -- does not compile as b' does not
-- b' = 10000 * c -- does not compile as c is undefined

-- Type variable or specific type constructor?

-- Choices are:
-- fully polymorphic type variable
-- constrained polymorphic type variable
-- concrete type constructor

-- f :: zed -> Zed -> Blah
--      [0]    [1]    [2]
-- [0] fully polymorphic type variable
-- [1] concrete type constructor
-- [2] concrete type constructor

-- f :: Enum b => a -> b -> C
--               [0]  [1]  [2]
-- [0] fully polymorphic type variable
-- [1] constrained (Enum) polymorphic type variable
-- [2] concrete type constructor

-- f :: f -> g -> C
--     [0]  [1]  [2]
-- [0] fully polymorphic type variable
-- [1] fully polymorphic type variable
-- [2] concrete type constructor

-- Write a type signature

functionH :: [a] -> a
functionH (x:_) = x

functionC :: Ord a => a -> a -> Bool
functionC x y = if (x > y) then True else False

functionS :: (a, b) -> b
functionS (x, y) = y

-- Given a type, write the function

i :: a -> a
i = id

c :: a -> b -> a
c x _ = x

c'' = c -- given alpha equivalence

c' :: a -> b -> b
c' _ y = y

r :: [a] -> [a]
r = tail

co :: (b -> c) -> (a -> b) -> a -> c
co bToc aTob a = bToc $ aTob a

aa :: (a -> c) -> a -> a
aa _ x = x

aa' :: (a -> b) -> a -> b
aa' f x = f x

-- Fix it

fstString :: [Char] -> [Char]
fstString xs = xs ++ " in the rain"

sndString :: [Char] -> [Char]
sndString xs = xs ++ " over the rainbow"

sing :: [Char]
sing = if (x > y) then (fstString x) else (sndString y)
    where x = "Singin"
          y = "Somewhere"

sing' :: [Char]
sing' = if (x < y) then (fstString x) else (sndString y)
    where x = "Singin"
          y = "Somewhere"


main :: IO ()
main = do
    print (1 + 2)
    putStrLn (show 10)
    print (negate (-1))
    print ((+) 0 blah)
        where blah = negate 1

-- Type-Kwon-Do

tkd_1_f :: Int -> String
tkd_1_f = undefined

tkd_1_g :: String -> Char
tkd_1_g = undefined

tkd_1_h :: Int -> Char
tkd_1_h n = tkd_1_g $ tkd_1_f n
-- tkd_1_h = tkd_1_g . tkd_1_f

data A
data B
data C

tkd_2_q :: A -> B
tkd_2_q = undefined

tkd_2_w :: B -> C
tkd_2_w = undefined

tkd_2_e :: A -> C
tkd_2_e a = tkd_2_w $ tkd_2_q a
-- tkd_2_e = tkd_2_w . tkd_2_q

data X
data Y
data Z

xz :: X -> Z
xz = undefined

yz :: Y -> Z
yz = undefined

xform :: (X, Y) -> (Z, Z)
xform (x, y) = (xz x, yz y)

munge :: (x -> y) -> (y -> (w,z)) -> x -> w
munge f g x = fst (g (f x))


