module Chapter_02 where

-- Parenthesization

paren_1 = 2 + 2 * 3 - 1
paren_1' = 2 + (2 * 3) -1

paren_2 = (^) 10 $ 1 + 1
paren_2' = 10 ^ (1 + 1)

paren_3 = 2 ^ 2 * 4 ^ 5 + 1
paren_3' = (2^2) * (4^5) + 1

-- Equivalent expressions

-- True
ee_1 = 1 + 1 == 2

-- True
ee_2 = 10 ^ 2 == 10 + 9 * 10

-- False
ee_3 = 400 - 37 == (-) 37 400

-- Type error
-- 100 `div` 3 :: Integral a => a
-- 100 / 3 :: Fractional a => a
-- ee_4 = 100 `div` 3 == 100 / 3

-- False
ee_5 = 2 * 5 + 18 == 2 * (5 + 18)


-- More fun with functions
-- We enter the values as if in the REPL: order matters
z = 7         -- does not depend on any other variable
y = z + 8     -- only depends on z
x = y ^ 2     -- depends on y
waxOn = x * 5 -- depends on x

-- Note that waxOn == 1125

mfwf_11 = 10 + waxOn == 1135
mfwf_12 = (+10) waxOn == 1135
mfwf_13 = (-) 15 waxOn == -1110
mfwf_14 = (-) waxOn 15 == 1110

triple x = x * 3

mfwf_3 = triple waxOn == 3375

waxOn' = x * 5 where
    x = y ^ 2
    y = z + 8
    z = 7

mfwf_4 = waxOn' == waxOn

-- We could write this one point free, of course
waxOff x = triple x

mfwf_71 = waxOff waxOn == 3375
mfwf_72 = waxOff (-50) == (-150)
mfwf_73 = waxOff (100 / 3) == 100

