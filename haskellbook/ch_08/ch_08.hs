module Chapter_08 where

import Data.List (intersperse)

-- Review of types

-- Q1. [[True, False], [True, True], [False, True]] :: [[Bool]]
-- Q2. [[3 == 3], [6 < 5], [3 < 4]] :: [[Bool]]
-- Q3. all of the above
-- Q4. func "Hello" "World"

-- Reviewing currying

cattyConny :: String -> String -> String
cattyConny x y = x ++ " mrow " ++ y

flippy :: String -> String -> String
flippy = flip cattyConny

appedCatty :: String -> String
appedCatty = cattyConny "woops"

frappe :: String -> String
frappe = flippy "haha"

rc_1 = appedCatty "woohoo!" == "woops mrow woohoo!"
rc_2 = frappe "1" == "1 mrow haha"
rc_3 = frappe (appedCatty "2") == "woops mrow 2 mrow haha"
rc_4 = appedCatty (frappe "blue") == "woops mrow blue mrow haha"
rc_5 = cattyConny (frappe "pink") (cattyConny "green" (appedCatty "blue"))
        == "pink mrow haha mrow green mrow woops mrow blue"
rc_6 = cattyConny (flippy "Pugs" "are") "awesome" == "are mrow Pugs mrow awesome"

-- Recursion

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
    where go n d count
             | n < d = (count, n)
             | otherwise = go (n - d) d (count + 1)

-- dividedBy 15 2 =
-- go 15 2 0
--   go 13 2 1
--     go 11 2 2
--       go 9 2 3
--         go 7 2 4
--           go 5 2 5
--             go 3 2 6
--               go 1 2 7
--               | 1 < 2 = (7, 1)
-- (7, 1)

sumAll :: (Eq a, Num a) => a -> a
sumAll 0 = 0
sumAll x = x + sumAll (x - 1)

multiply :: (Integral a) => a -> a -> a
multiply x 1 = x
multiply x y = x + multiply x (y -1)

-- Fixing dividedBy

data DividedResult = Result Integer | DividedByZero deriving (Show)

dividedBy' :: Integral a => a -> a -> DividedResult
dividedBy' x y
    | y == 0 = DividedByZero
    | x * y < 0 = Result (negate (go (abs x) (abs y) 0))
    | otherwise = Result (go (abs x) (abs y) 0)
    where go x y count | abs x < y = count | otherwise = go (x - y) y (count + 1)

mc91 :: Integral a => a -> a
mc91 x
    | x > 100 = x - 10
    | otherwise = (mc91 . mc91) (x + 11)

-- Number into words

digitToWord :: Int -> String
digitToWord n =
    case n of
        0 -> "zero"
        1 -> "one"
        2 -> "two"
        3 -> "three"
        4 -> "four"
        5 -> "five"
        6 -> "six"
        7 -> "seven"
        8 -> "eight"
        9 -> "nine"

digits :: Int -> [Int]
digits n
    | n < 10 = [n]
    | otherwise = digits (d) ++ [m]
    where (d, m) = divMod n 10

-- variant below avoids the use of the (++) operator
digits' :: Int -> [Int]
digits' = reverse . go
    where go n
           | n < 10 = [n]
           | otherwise = m : go (d)
           where (d, m) = divMod n 10

wordNumber :: Int -> String
wordNumber = concat . intersperse "-" . map digitToWord . digits



