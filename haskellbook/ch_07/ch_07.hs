module Chapter_07 where

-- Grab Bag p.224
-- all versions are equivalent

mTh x y z = x * y * z

mTh' x y = \z -> x * y * z

mTh'' x = \y -> \z -> x * y * z

mTh''' = \x -> \y -> \z -> x * y * z

-- the type of mTh 3 is
-- Num a => a -> a -> a

addOneIfOdd n = case odd n of
    True -> f n
    False -> n
    where f = \n -> n + 1

addFive = \x -> \y -> (if x > y then y else x) + 5

mflip f x y = f y x

-- Variety pack

k :: (a, b) -> a
k (x, y) = x

k1 :: Num a => a
k1 = k ((4 - 1), 10)

k2 :: [Char]
k2 = k ("three", (1 + 2))

k3 :: Num a => a
k3 = k (3, True)

-- k1 and k3 yield 3 as a result

f :: (a, b, c) -> (d, e, f) -> ((a, d), (c, f))
f (a, b, c) (d, e, f) = ((a, d), (c, f))

-- Case Practice

functionC x y =
    case x > y of
        True  -> x
        False -> y

ifEvenAdd2 n =
    case even n of
        True  -> n+2
        False -> n

nums x =
    case compare x 0 of
        LT -> -1
        GT -> 1
        EQ -> 0

-- Artful Dodgy

dodgy :: Num a => a -> a -> a
dodgy x y = x + y * 10

oneIsOne = dodgy 1
oneIsTwo = (flip dodgy) 2

-- Guard Duty

-- yields True when xs is a palindrome
pal :: Eq a => [a] -> Bool
pal xs
    | xs == reverse xs = True
    | otherwise = False

-- yields an indication of whether its argument is a
-- positive or negative number or zero
numbers :: (Num a, Eq a, Ord a) => a -> Int
numbers x
    | x < 0 = -1
    | x == 0 = 0
    | x > 0 = 1

-- Chapter exercises

-- A polymorphic function may resolve to values of different types, depending on input
-- The composed function of g . f has the type Char -> [String]
-- (Ord a, Num a) => a -> Bool
-- A function with the type (a -> b) -> c is a higher order function
-- f True :: Bool

-- Let's write code

tensDigit :: Integral a => a -> a
tensDigit x = d
    where xLast = x `div` 10
          d     = xLast `mod` 10

tensDigit' :: Integral a => a -> a
tensDigit' x = fst $ divMod x 10

hunsD :: Integral a => a -> a
hunsD x = fst $ divMod x 100

hunsD' :: Integral a => a -> a
hunsD' = tensDigit' . tensDigit'

foldBool :: a -> a -> Bool -> a
foldBool x y b =
    case b of
        True  -> x
        False -> y

foldBool' :: a -> a -> Bool -> a
foldBool' x y b
    | b == True  = x
    | b == False = y

g :: (a -> b) -> (a, c) -> (b, c)
g f (x, y) = (f x, y)

roundTrip :: (Read a, Show a) => a -> a
roundTrip = read . show

roundTrip' :: (Show a, Read b) => a -> b
roundTrip' x = read  (show x)

-- print (roundTrip' 4 :: Int)





