module Chapter_09 where

import Data.Bool
import Data.Char

-- EnumFromTo p. 304

eftEnum :: (Eq a, Ord a, Enum a) => a -> a -> [a]
eftEnum x y
    | x < y     = x : eftEnum (succ x) y
    | x == y    = [x]
    | otherwise = []

eftBool :: Bool -> Bool -> [Bool]
eftBool = eftEnum

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd = eftEnum

eftInt :: Int -> Int -> [Int]
eftInt = eftEnum

eftChar :: Char -> Char -> [Char]
eftChar = eftEnum

-- Thy Fearful Symmetry p.307

mySplitAt :: Char -> String -> [String]
mySplitAt c xs
    | xs == []  = []
    | otherwise = takeWhile (/=c) xs : mySplitAt c (dropWhile (==c) (dropWhile (/=c) xs))

-- Comprehend Thy Lists p.312

mySqr = [x^2 | x <- [1..10]]

ctl_1 = [x | x <- mySqr, rem x 2 == 0] == [4, 16, 36, 64, 100]
ctl_2 = [(x,y) | x <- mySqr, y <- mySqr, x < 50, y > 50] 
            == [(1,64),(1,81),(1,100),
                (4,64),(4,81),(4,100),
                (9,64),(9,81),(9,100),
                (16,64),(16,81),(16,100),
                (25,64),(25,81),(25,100),
                (36,64),(36,81),(36,100),
                (49,64),(49,81),(49,100)]
ctl_3 = take 5 [(x,y) | x <- mySqr, y <- mySqr, x < 50, y > 50]
            == [(1,64),(1,81),(1,100),(4,64),(4,81)]

-- Square Cube p.313

myCube = [y^3 | y <- [1..10]]

sc_1 = [(x,y) | x <- mySqr, y <- myCube]
sc_2 = [(x,y) | x <- mySqr, y <- myCube, x < 50, y < 50]
sc_3 = length sc_2

-- Will it blow up? p.322

-- [x^y | x <- [1..5], y <- [2, undefined]] -- Boom
wibu_2 = take 1 $ [x^y | x <- [1..5], y <- [2, undefined]]
-- sum [1, undefined, 3] -- Boom
wibu_4 = length [1, undefined, 3]
-- length $ [1, 2, 3] ++ undefined -- Boom
wibu_6 = take 1 $ filter even [1, 2, 3, undefined]
-- wibu_7 = take 1 $ filter even [1, 3, undefined] -- Boom
wibu_8 = take 1 $ filter odd [1, 3, undefined]
wibu_9 = take 2 $ filter odd [1, 3, undefined]
-- take 3 $ filter odd [1, 3, undefined] -- Boom

-- Is it in normal form? p.322
nf_1 = [1, 2, 3, 4, 5] -- NF
-- nf_2 = 1 : 2 : 3 : 4 : _ -- WHNF
nf_3 = enumFromTo 1 10 -- neither
nf_4 = length [1, 2, 3, 4, 5] -- neither
nf_5 = sum (enumFromTo 1 10) -- neither
nf_6 = ['a'..'m'] ++ ['n'..'z'] -- neither
-- nf_7 = (_, 'b') -- WHNF

-- More bottoms p.329

-- take 1 $ map (+1) [undefined, 2, 3] --Boom
mb_2 = take 1 $ map (+1) [1, undefined, 3]
-- take 2 $ map (+1) [1, undefined, 3] -- Boom
-- itIsMystery takes a String and returns a list of Bools
-- True for the vowels and False for the consonants
itIsMystery :: [Char] -> [Bool]
itIsMystery xs = map (\x -> elem x "aeiou") xs
mb_5a = map (^2) [1..10] == [1,4,9,16,25,36,49,64,81,100]
mb_5b = map minimum [[1..10], [10..20], [20..30]] == [1, 10, 20]
mb_5c = map sum [[1..5], [1..5], [1..5]] == [15, 15, 15]

mb_6  = map (\x -> bool (-x) x (x==3))

-- Filtering p.331

f_1 = filter (\x -> x `mod` 3 == 0)
f_2 = length . f_1
f_3 :: String -> [String]
f_3 = filter (\x -> not (elem x ["the","a","an"])) . words

-- Zipping exercises p.333

myZip :: [a] -> [b] -> [(a, b)]
myZip _ [] = []
myZip [] _ = []
myZip (x:xs) (y:ys) = (x, y) : myZip xs ys

myZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myZipWith f _ [] = []
myZipWith f [] _ = []
myZipWith f (x:xs) (y:ys) = f x y : myZipWith f xs ys

myZip' :: [a] -> [b] -> [(a, b)]
myZip' = myZipWith (\x y -> (x, y))

-- End of chapter exercises

-- Data.Char
-- isUpper :: Char -> Bool
-- toUpper :: Char -> Char

filterU :: String -> String
filterU = filter isUpper

capitalizeIt :: String -> String
capitalizeIt [] = []
capitalizeIt (x:xs) = toUpper x : xs

stringToUpper :: String -> String
stringToUpper [] = []
stringToUpper (x:xs) = toUpper x : stringToUpper xs

firstToUpper = toUpper . head

-- Ciphers, see the ciphers.hs file

-- Standard functions

myOr :: [Bool] -> Bool
myOr [] = False
myOr (x:xs) = x || myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny f [] = False
myAny f (x:xs) = f x || myAny f xs

myElem :: Eq a => a -> [a] -> Bool
myElem e [] = False
myElem e (x:xs) = (e == x) || myElem e xs

myElem' :: Eq a => a -> [a] -> Bool
myElem' x = myAny (== x)

myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

squish :: [[a]] -> [a]
squish [] = []
squish (x:xs) = x ++ squish xs

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f [] = []
squishMap f (x:xs) = f x ++ squishMap f xs

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy f [a] = a
myMaximumBy f (x:xs)
    | f x (myMaximumBy f xs) == GT = x
    | otherwise                    = myMaximumBy f xs

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f [a] = a
myMinimumBy f (x:xs)
    | f x (myMinimumBy f xs) == LT = x
    | otherwise                    = myMinimumBy f xs

myMaximum :: (Ord a) => [a] -> a
myMaximum = myMaximumBy compare

myMinimum :: (Ord a) => [a] -> a
myMinimum = myMinimumBy compare


