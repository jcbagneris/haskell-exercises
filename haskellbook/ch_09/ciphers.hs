module Cipher where

import Data.Char

caesar :: Int -> String -> String
caesar n xs = map (\x -> chr(mod (ord x - 32 + n) 91 + 32)) xs

unCaesar :: Int -> String -> String
unCaesar n = caesar (-n)
