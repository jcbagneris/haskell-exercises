import Test.Hspec

import Golf

main :: IO ()
main = hspec $ do
  describe "Golf" $ do
    describe "skips" $ do
        it "skips [1] == [[1]]" $
            skips [1] `shouldBe` [[1]]

        it "skips [True, False] == [[True, False], [False]]" $
            skips [True, False] `shouldBe` [[True, False], [False]]

        it "skips \"ABCD\" == [\"ABCD\", \"BD\", \"C\", \"D\"]" $
            skips "ABCD" `shouldBe` ["ABCD", "BD", "C", "D"]

        it "skips \"hello!\" == [\"hello!\", \"el!\", \"l!\", \"l\", \"o\", \"!\"]" $
            skips "hello!" `shouldBe` ["hello!", "el!", "l!", "l", "o", "!"]

    describe "localMaxima" $ do
        it "localMaxima [] == []" $ do
            localMaxima [] `shouldBe` []

        it "localMaxima [1] == []" $ do
            localMaxima [1] `shouldBe` []

        it "localMaxima [2,1] == []" $ do
            localMaxima [2,1] `shouldBe` []

        it "localMaxima [2,3,1] == [3]" $ do
            localMaxima [2,3,1] `shouldBe` [3]

        it "localMaxima [2,9,5,6,1] == [9,6]" $ do
            localMaxima [2,9,5,6,1] `shouldBe` [9,6]

        it "localMaxima [2,3,4,1,5] == [4]" $ do
            localMaxima [2,3,4,1,5] `shouldBe` [4]

        it "localMaxima [1,2,3,4,5] == []" $ do
            localMaxima [1,2,3,4,5] `shouldBe` []

    describe "histogram" $ do
        it "histogram [] == \"==========\\n0123456789\\n\"" $ do
            histogram [] `shouldBe` "==========\n0123456789\n"

        it "histogram [3,5] == \"\"" $ do
            histogram [3,5] `shouldBe` ""

        it "histogram [1,1,1,5] == \"\"" $ do
            histogram [1,1,1,5] `shouldBe` ""


