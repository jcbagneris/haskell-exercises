{-# OPTIONS_GHC -Wall #-}
module Golf where

import Data.List

skips :: [a] -> [[a]]
skips xs = map (every xs) [1..length xs]

every :: [a] -> Int -> [a]
every xs n
  | null (take' n xs) = []
  | otherwise         = last (take' n xs) : every (drop n xs) n

take' :: Int -> [a] -> [a]
take' n xs = if length xs >= n then take n xs else []

localMaxima :: [Integer] -> [Integer]
localMaxima = concatMap m . tr

tr :: [Integer] -> [[Integer]]
tr [] = []
tr xs = take' 3 xs : tr (drop 1 xs)

m :: [Integer] -> [Integer]
m [x,y,z] = if y>x && y>z then [y] else []
m _ = []

histogram :: [Integer] -> String
histogram = concat . reverse. transpose . h
-- construire à plat et transposer

h :: [Integer] -> [String]
h [] = ["==========\n","0123456789\n"]
h (x:xs) = k x : h xs

k :: Integer -> String
k n = genericTake n (repeat ' ') ++ "*" ++ genericTake (9-n) (repeat ' ') ++ "\n"

order :: [Integer] -> [Integer]
order [] = []

takeOne :: [[Integer]] -> [[Integer]]
takeOne [[]] = [[]]
takeOne ((x:xs):ys) = [[]]

