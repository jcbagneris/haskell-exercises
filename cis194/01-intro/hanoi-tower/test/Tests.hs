import Test.Hspec

import HanoiTower (hanoi, hanoi4)

main :: IO ()
main = hspec $ do
  describe "HanoiTower" $ do
    describe "hanoi" $ do
        it "hanoi 2 \"a\" \"b\" \"c\" == [(\"a\",\"c\"), (\"a\",\"b\"), (\"c\",\"b\")]" $
            hanoi 2 "a" "b" "c" `shouldBe` [("a","c"), ("a","b"), ("c","b")]

        it "length $ hanoi 15 \"a\" \"b\" \"c\" == 32767" $
            length (hanoi 15 "a" "b" "c") `shouldBe` 32767

    describe "hanoi4" $ do
        it "length $ hanoi4 15 \"a\" \"b\" \"c\" \"d\" == 129" $
            length (hanoi4 15 "a" "b" "c" "d") `shouldBe` 129
