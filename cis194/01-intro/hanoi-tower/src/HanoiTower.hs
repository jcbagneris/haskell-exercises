module HanoiTower (hanoi, hanoi4) where

type Peg = String

type Move = (Peg, Peg)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 0 _ _ _ = []
hanoi n a b c = hanoi (n-1) a c b ++ [(a, b)] ++ hanoi (n-1) c b a

hanoi4 :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
-- to move the disks from a to b
-- move n-2 disks from a to d using b and c as temp storage
-- move the 2 remaining disks from a to b using c as temp storage
-- move n-2 disks from d to b using a and c as temp storage
--
-- this solution is still not optimal, unfortunately
-- length (hanoi4 15 "a" "b" "c" "d") == 145
hanoi4 1 a b _ _ = [(a, b)]
hanoi4 2 a b c _ = hanoi 2 a b c
hanoi4 3 a b c d = [("a","d"),("a","c"),("a","b"),("c","b"),("d","b")]
hanoi4 4 a b c d = [("a","b"),("a","d"),("b","d"),("a","c"),("a","b"),("c","b"),("d","a"),("d","b"),("a","b")] 
hanoi4 n a b c d = hanoi4 (n-4) a d b c ++ hanoi 4 a b c ++ hanoi4 (n-4) d b a c

-- the proposition below comes from https://github.com/potatosalad/cis194
-- though interesting, it is not better than mine, and actually performs
-- worse
-- length (hanoi4' 15 "a" "b" "c" "d") == 305
hanoin :: Integer -> [Peg] -> [Move]
hanoin 0 _ = []
hanoin 1 (a:b:_) = [(a, b)]
hanoin x (a:b:c:rest) =
    hanoin k (a:c:b:rest) ++
    hanoin (x-k) (a:b:rest) ++
    hanoin k (c:b:a:rest)
        where k = if (null rest) then (x-1) else (x `div` 2)

hanoi4' :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
hanoi4' x a b c d = hanoin x [a, b, c, d]
