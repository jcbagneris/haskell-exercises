module CreditCardValidation (toDigits
                            ,toDigitsRev
                            ,doubleEveryOther
                            ,sumDigits
                            ,hashSum
                            ,validate) where

toDigits :: Integer -> [Integer]
toDigits = reverse . toDigitsRev

toDigitsRev :: Integer -> [Integer]
toDigitsRev i
  | i <= 0    = []
  | otherwise = (i `mod` 10) : toDigitsRev (i `div` 10)

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther = reverse . doubleEveryOtherFromLeft . reverse

doubleEveryOtherFromLeft :: [Integer] -> [Integer]
doubleEveryOtherFromLeft []       = []
doubleEveryOtherFromLeft [x]      = [x]
doubleEveryOtherFromLeft (x:y:xs) = x : 2*y : doubleEveryOtherFromLeft xs

sumDigits :: [Integer] -> Integer
sumDigits = sum . concatMap toDigits 

validate :: Integer -> Bool
validate x = hashSum x `rem` 10 == 0

hashSum :: Integer -> Integer
hashSum = sumDigits . doubleEveryOtherFromLeft . toDigitsRev
