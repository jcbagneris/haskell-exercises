import Test.Hspec

import CreditCardValidation (toDigits
                            ,toDigitsRev
                            ,doubleEveryOther
                            ,sumDigits
                            ,hashSum
                            ,validate)

main :: IO ()
main = hspec $ do
  describe "CreditCardValidation" $ do
    describe "toDigitsRev" $ do
        it "toDigitsRev 1386 == [6,8,3,1]" $
            toDigitsRev 1386 `shouldBe` [6,8,3,1]

        it "toDigitsRev 0 == []" $
            toDigitsRev 0 `shouldBe` []

        it "toDigitsRev (-17) == []" $
            toDigitsRev (-17) `shouldBe` []

    describe "toDigits" $ do
        it "toDigits 1234 == [1,2,3,4]" $
            toDigits 1234 `shouldBe` [1,2,3,4]

        it "toDigits 0 == []" $
            toDigits 0 `shouldBe` []

        it "toDigits (-3846) == []" $
            toDigits (-3846) `shouldBe` []

    describe "doubleEveryOther" $ do
        it "doubleEveryOther [8,7,6,5] == [16,7,12,5]" $
            doubleEveryOther [8,7,6,5] `shouldBe` [16,7,12,5]

        it "doubleEveryOther [1,2,3] == [1,4,3]" $
            doubleEveryOther [1,2,3] `shouldBe` [1,4,3]

        it "doubleEveryOther [] == []" $
            doubleEveryOther [] `shouldBe` []

        it "doubleEveryOther [3] == [3]" $
            doubleEveryOther [3] `shouldBe` [3]

        it "doubleEveryOther [8,9] == [16,9]" $
            doubleEveryOther [8,9] `shouldBe` [16,9]

    describe "sumDigits" $ do
        it "sumDigits [16,7,12,5] == 22" $
            sumDigits [16,7,12,5] `shouldBe` 22

        it "sumDigits [2,3,16,6] == 18" $
            sumDigits [2,3,16,6] `shouldBe` 18

    describe "hashSum" $ do
        it "hashSum 1386 == 18" $
            hashSum 1386 `shouldBe` 18

    describe "validate" $ do
        it "validate 4012888888881881 == True" $
            validate 4012888888881881 `shouldBe` True

        it "validate 4012888888881882 == False" $
            validate 4012888888881882 `shouldBe` False
