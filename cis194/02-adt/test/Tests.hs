import Test.Hspec

import LogAnalysis
import Log

main :: IO ()
main = hspec $ do
  describe "LogAnalysis" $ do
    describe "parseMessage" $ do
        it "parseMessage \"\" == Unknown \"\"" $
            parseMessage "" `shouldBe` Unknown ""

        it "parseMessage \"E\" == Unknown \"E\"" $
            parseMessage "E" `shouldBe` Unknown "E"

        it "parseMessage \"E blah foo bar\" == Unknown \"E blah foo bar\"" $
            parseMessage "E blah foo bar" `shouldBe` Unknown "E blah foo bar"

        it "parseMessage \"E 314\" == Unknown \"E 314\"" $
            parseMessage "E 314" `shouldBe` Unknown "E 314"

        it "parseMessage \"E 5 26\" == LogMessage (Error 5) 26 \"\"" $
            parseMessage "E 5 26" `shouldBe` LogMessage (Error 5) 26 ""

        it "parseMessage \"E 2 562 help help\" == LogMessage (Error 2) 562 \"help help\"" $
            parseMessage "E 2 562 help help" `shouldBe` LogMessage (Error 2) 562 "help help"

        it "parseMessage \"I\" == Unknown \"I\"" $
            parseMessage "I" `shouldBe` Unknown "I"

        it "parseMessage \"I blah foo bar\" == Unknown \"I blah foo bar\"" $
            parseMessage "I blah foo bar" `shouldBe` Unknown "I blah foo bar"

        it "parseMessage \"I 45\" == LogMessage Info 45 \"\"" $
            parseMessage "I 45" `shouldBe` LogMessage Info 45 ""

        it "parseMessage \"I 29 la la la\" == LogMessage Info 29 \"la la la\"" $
            parseMessage "I 29 la la la" `shouldBe` LogMessage Info 29 "la la la"

        it "parseMessage \"W\" == Unknown \"W\"" $
            parseMessage "W" `shouldBe` Unknown "W"

        it "parseMessage \"W blah foo bar\" == Unknown \"W blah foo bar\"" $
            parseMessage "W blah foo bar" `shouldBe` Unknown "W blah foo bar"

        it "parseMessage \"W 7112\" == LogMessage Warning 7112 \"\"" $
            parseMessage "W 7112" `shouldBe` LogMessage Warning 7112 ""

        it "parseMessage \"W 434 foo bar\" == LogMessage Warning 434 \"foo bar\"" $
            parseMessage "W 434 foo bar" `shouldBe` LogMessage Warning 434 "foo bar"

        it "parseMessage \"This is not in the right format\" == Unknown \"This is not in the right format\"" $
            parseMessage "This is not in the right format" `shouldBe` Unknown "This is not in the right format"

    describe "parse" $ do
        it "should parse the sample.log file" $ do
            contents <- readFile "data/sample.log"
            parse contents `shouldBe` [LogMessage Info 6 "Completed armadillo processing"
                                      ,LogMessage Info 1 "Nothing to report"
                                      ,LogMessage Info 4 "Everything normal"
                                      ,LogMessage Info 11 "Initiating self-destruct sequence"
                                      ,LogMessage (Error 70) 3 "Way too many pickles"
                                      ,LogMessage (Error 65) 8 "Bad pickle-flange interaction detected"
                                      ,LogMessage Warning 5 "Flange is due for a check-up"
                                      ,LogMessage Info 7 "Out for lunch, back in two time steps"
                                      ,LogMessage (Error 20) 2 "Too many pickles"
                                      ,LogMessage Info 9 "Back from lunch"
                                      ,LogMessage (Error 99) 10 "Flange failed!"
                                      ]

    describe "insert" $ do
        it "should ignore unknown messages" $ do
            insert (Unknown "") Leaf `shouldBe` Leaf

        it "should insert a log message in an empty MessageTree" $ do
            insert (LogMessage Warning 4 "foo") Leaf `shouldBe` (Node Leaf (LogMessage Warning 4 "foo") Leaf)

        it "should insert log messages based on TimeStamp in the sorted MessageTree" $ do
            insert (LogMessage Warning 4 "blah") (Node 
                                                   (Node Leaf (LogMessage (Error 404) 1 "Not found") Leaf)
                                                   (LogMessage Info 3 "foo")
                                                   (Node Leaf (LogMessage Warning 5 "bar") Leaf)
                                                 ) 
                                                 `shouldBe`
                                                 (Node
                                                   (Node Leaf (LogMessage (Error 404) 1 "Not found") Leaf)
                                                   (LogMessage Info 3 "foo")
                                                   (Node
                                                     (Node Leaf (LogMessage Warning 4 "blah") Leaf)
                                                     (LogMessage Warning 5 "bar")
                                                     Leaf
                                                   )
                                                 )

    describe "build" $ do
        it "should build a known tree from the sample.log file" $ do
            contents <- readFile "data/sample.log"
            build (parse contents) `shouldBe` (Node
                                                (Node
                                                  (Node
                                                    (Node Leaf (LogMessage Info 1 "Nothing to report") Leaf)
                                                    (LogMessage (Error 20) 2 "Too many pickles")
                                                    (Node
                                                      (Node
                                                        (Node Leaf
                                                              (LogMessage (Error 70) 3 "Way too many pickles")
                                                              (Node Leaf (LogMessage Info 4 "Everything normal") Leaf)
                                                        )
                                                        (LogMessage Warning 5 "Flange is due for a check-up")
                                                        (Node Leaf
                                                        (LogMessage Info 6 "Completed armadillo processing") Leaf)
                                                      )
                                                      (LogMessage Info 7 "Out for lunch, back in two time steps")
                                                      (Node Leaf (LogMessage (Error 65) 8 "Bad pickle-flange interaction detected") Leaf)
                                                    )
                                                  )
                                                  (LogMessage Info 9 "Back from lunch")
                                                  Leaf)
                                                (LogMessage (Error 99) 10 "Flange failed!")
                                                (Node Leaf (LogMessage Info 11 "Initiating self-destruct sequence") Leaf)
                                              )
    describe "inOrder" $ do
        it "should sort the contents of the sample.log file" $ do
            contents <- readFile "data/sample.log"
            inOrder (build (parse contents)) `shouldBe` [LogMessage Info 1 "Nothing to report"
                                                        ,LogMessage (Error 20) 2 "Too many pickles"
                                                        ,LogMessage (Error 70) 3 "Way too many pickles"
                                                        ,LogMessage Info 4 "Everything normal"
                                                        ,LogMessage Warning 5 "Flange is due for a check-up"
                                                        ,LogMessage Info 6 "Completed armadillo processing"
                                                        ,LogMessage Info 7 "Out for lunch, back in two time steps"
                                                        ,LogMessage (Error 65) 8 "Bad pickle-flange interaction detected"
                                                        ,LogMessage Info 9 "Back from lunch"
                                                        ,LogMessage (Error 99) 10 "Flange failed!"
                                                        ,LogMessage Info 11 "Initiating self-destruct sequence"
                                                        ]

    describe "whatWentWrong" $ do
        it "should get a known errors list from the sample.log file" $ do
            contents <- readFile "data/sample.log"
            whatWentWrong (inOrder (build (parse contents))) `shouldBe` ["Way too many pickles"
                                                                        ,"Bad pickle-flange interaction detected"
                                                                        ,"Flange failed!"
                                                                        ]

