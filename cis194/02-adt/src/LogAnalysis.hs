{-# OPTIONS_GHC -Wall #-}
module LogAnalysis (parseMessage
                   ,parse
                   ,insert
                   ,build
                   ,inOrder
                   ,whatWentWrong
                   )

where

{-
  Notes:
  - all functions are total, no warnings left
  - parsing correctly handles incorrect messages (no timestamp, no error number)
  - full tests in test/Tests.hs
  - to find the answer to exercise 6, take the first letters of all severe error strings
    from data/error.log
-}

import Log
import Data.Char (isNumber)

parseMessage :: String -> LogMessage
parseMessage = parseWords . words

parseWords :: [String] -> LogMessage
parseWords xs@("E":es:ts:rs) = parseErrNum xs es ts (LogMessage (Error 0) 0 (unwords rs))
parseWords xs@("W":ts:rs) = parseTimeStamp xs ts (LogMessage Warning 0 (unwords rs))
parseWords xs@("I":ts:rs) = parseTimeStamp xs ts (LogMessage Info 0 (unwords rs))
parseWords ls        = Unknown $ unwords ls

parseTimeStamp :: [String] -> String -> LogMessage -> LogMessage
parseTimeStamp ls ts (LogMessage t _ xs)
  | all isNumber ts = LogMessage t (read ts) xs
  | otherwise       = Unknown (unwords ls)
parseTimeStamp xs _ (Unknown _)          = Unknown (unwords xs)

parseErrNum :: [String] -> String -> String -> LogMessage -> LogMessage
parseErrNum ls es ts (LogMessage _ _ xs)
  | all isNumber es = parseTimeStamp ls ts (LogMessage (Error (read es)) 0 xs)
  | otherwise       = Unknown (unwords ls)
parseErrNum xs _ _ (Unknown _)           = Unknown (unwords xs)

parse :: String -> [LogMessage]
parse = map parseMessage . lines

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) mt = mt
insert lm Leaf = Node Leaf lm Leaf
insert lm@(LogMessage _ ts _) mt@(Node left m right) = if ts < getTimeStamp mt
                                                       then (Node (insert lm left) m right)
                                                       else (Node left m (insert lm right))

getTimeStamp :: MessageTree -> TimeStamp
getTimeStamp (Node _ (LogMessage _ ts _) _) = ts
getTimeStamp _                              = 0

build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left m right) = inOrder left ++ [m] ++ inOrder right

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map messageString . filter severeError

messageString :: LogMessage -> String
messageString (LogMessage _ _ xs) = xs
messageString _                   = ""

severeError :: LogMessage -> Bool
severeError (LogMessage (Error s) _ _) = s >= 50
severeError _                          = False

main :: IO ()
main = do
    contents <- readFile "data/error.log"
    putStrLn (unlines (whatWentWrong (inOrder (build (parse contents)))))

