module CollatzConjecture (collatz) where

isEven :: Integer -> Bool
isEven x = x `mod` 2 == 0

collatz :: Integer -> Maybe Integer
collatz x
    | x <= 0 = Nothing
    | otherwise = Just (numSteps x)

numSteps :: Integer -> Integer
numSteps x
    | x == 1 = 0
    | isEven x = 1 + numSteps (x `div` 2)
    | otherwise = 1 + numSteps (x * 3 + 1)
