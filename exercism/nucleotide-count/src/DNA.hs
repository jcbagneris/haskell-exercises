module DNA (nucleotideCounts) where

import Data.Map (Map, size, fromList, insertWith)

nucleotideCounts :: String -> Either String (Map Char Int)
nucleotideCounts (xs)
    | size n > 4            = Left "Invalid strand!"
    | otherwise             = Right n
    where n = countGATC xs

-- Let's try to do this one by parsing the DNA strand only once :)

countGATC :: String -> (Map Char Int)
countGATC [] = fromList [('A', 0), ('C', 0), ('G', 0), ('T', 0)]
countGATC (x:xs) = insertWith (+) x 1 (countGATC xs)

