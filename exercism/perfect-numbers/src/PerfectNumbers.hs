module PerfectNumbers (classify, Classification(..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

classify :: Int -> Maybe Classification
classify n
    | n <= 0    = Nothing
    | sf == n   = Just Perfect
    | sf < n    = Just Deficient
    | otherwise = Just Abundant
    where sf = sum $ factors n

factors :: Int -> [Int]
factors n = [x | x <- [1..(n `div` 2)], n `mod` x == 0]

