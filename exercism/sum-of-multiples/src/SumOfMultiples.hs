module SumOfMultiples (sumOfMultiples) where

import Data.List (nub)

sumOfMultiples :: [Integer] -> Integer -> Integer
sumOfMultiples factors limit = sum $ nub multiples
    where multiples = [x*y | x <- [1..limit], y <- factors, x*y < limit]
