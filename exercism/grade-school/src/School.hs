module School (School, add, empty, grade, sorted) where

import Data.Maybe (fromJust)
import Data.List (sort, insert)
import Data.Map (Map, empty, insertWith, toList, (!?))

type School = Map Int [String]

add :: Int -> String -> School -> School
add gradeNum student school = insertWith (insStudent) gradeNum [student] school

insStudent :: [String] -> [String] -> [String]
insStudent [student] students = insert student students

-- empty is provided by Data.Map

grade :: Int -> School -> [String]
grade gradeNum school
    | x == Nothing = []
    | otherwise    = fromJust x
    where x = school !? gradeNum

sorted :: School -> [(Int, [String])]
sorted  school = sort $ toList school
