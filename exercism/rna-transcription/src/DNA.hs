module DNA (toRNA) where

isDNA :: String -> Bool
isDNA xs = all (`elem` "GATC") xs

complement :: Char -> Char
complement 'G' = 'C'
complement 'C' = 'G'
complement 'T' = 'A'
complement 'A' = 'U'

toRNA :: String -> Maybe String
toRNA xs 
    | isDNA xs = Just (map complement xs)
    | otherwise = Nothing

