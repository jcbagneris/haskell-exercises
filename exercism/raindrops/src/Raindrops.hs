module Raindrops (convert) where

convert :: Int -> String
convert n = if null ppp then show n else ppp 
    where
      ppp = concat $ scanr (\(k,v) acc -> if n `mod` k == 0 then v else "") "" table
      table = [(3,"Pling"),(5,"Plang"),(7,"Plong")]

