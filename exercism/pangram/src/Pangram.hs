module Pangram (isPangram) where

import Data.Char (toLower)
import qualified Data.Set as Set

ascLetters = Set.fromList ['a'..'z']

isPangram :: String -> Bool
isPangram x = ascLetters `Set.isSubsetOf` xus
    where xus = Set.fromList (map toLower x)
