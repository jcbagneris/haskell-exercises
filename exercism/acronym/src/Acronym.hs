module Acronym (abbreviate) where

import Data.Char (
                    toUpper
                  , isSpace
                  , isLetter
                  , isUpper
                  , generalCategory
                  , GeneralCategory(DashPunctuation)
                  )

abbreviate :: String -> String
abbreviate xs = [toUpper (head x) | x <- sepWords xs]

isNotSeparator :: Char -> Bool
isNotSeparator c = not (isUpper c) && not (isSpace c) && generalCategory c /= DashPunctuation

sepWords :: String -> [String]
sepWords "" = []
sepWords (x:xs)
    | not (isLetter x) = sepWords xs
    | isUpper x && isUpper (head xs) = (x: takeWhile isUpper xs) : sepWords (dropWhile isUpper xs)
    | otherwise = (x: takeWhile isNotSeparator xs) : sepWords (dropWhile isNotSeparator xs)

