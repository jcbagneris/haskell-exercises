module Isogram (isIsogram) where

import qualified Data.Char as C
import qualified Data.List as L

isIsogram :: String -> Bool
isIsogram xs = L.nub cxs == cxs
    where cxs = filter C.isLetter $ map C.toLower xs

