module Diamond (diamond) where

import Data.Char

diamond :: Char -> Maybe [String]
diamond x
  -- degenerate cases
  | x == 'A' = Just ["A"]
  | x == 'B' = Just [" A ","B B"," A "]
  -- normal cases
  | x `elem` ['C'..'Z'] = Just (top x ++ middle x ++ bottom x)
  -- invalid input
  | otherwise = Nothing

top :: Char -> [String]
top x = lineA x ++ [line c x | c <- ['B'.. chr (ord x - 1)]]

bottom :: Char -> [String]
bottom x = reverse (top x)

middle :: Char -> [String]
middle x = [line x x]

line :: Char -> Char -> String
line x last = lr ++ [x] ++ middle ++ [x] ++ lr
  where
      middle = replicate (3 + (ord x - 67) * 2) ' '
      lr = replicate ((linelength last - linelength x) `div` 2) ' '

linelength :: Char -> Int
linelength last = 2 * (ord last - 64) - 1

lineA :: Char -> [String]
lineA x = [lr ++ "A" ++ lr]
  where
      lr = replicate ((linelength x - 1) `div` 2) ' '
