module Phone (number) where

import Data.Char (isDigit)

number :: String -> Maybe String
number xs  = Just xs >>= cleanIt >>= checkL >>= checkCC >>= checkEC >>= checkAC

-- remove everything but the digits
cleanIt :: String -> Maybe String
cleanIt xs = Just (filter isDigit xs)

-- check the length
checkL :: String -> Maybe String
checkL xs
  | length xs == 10 = Just xs
  | length xs == 11 = Just xs
  | otherwise       = Nothing

-- check and remove the country code
checkCC :: String -> Maybe String
checkCC xs
  | length xs == 11 = if head xs == '1' then Just (tail xs) else Nothing
  | otherwise       = Just xs

-- check the area code
checkAC :: String -> Maybe String
checkAC xs
  | head xs == '0' = Nothing
  | head xs == '1' = Nothing
  | otherwise      = Just xs

-- check the exchange code
checkEC :: String -> Maybe String
checkEC xs
  | xs !! 3 == '0' = Nothing
  | xs !! 3 == '1' = Nothing
  | otherwise      = Just xs

