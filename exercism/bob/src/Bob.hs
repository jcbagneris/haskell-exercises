module Bob (responseFor) where

import Data.Char

{-
-- These actually exist already in Data.Char

isLetter :: Char -> Bool
isLetter x = toLower x `elem` ['a'..'z']

isSpace :: Char -> Bool
isSpace x = elem x [' ','\n','\t','\r']
-}

isSilence :: String -> Bool
isSilence = all isSpace

hasLetters :: String -> Bool
hasLetters = any isLetter

rStrip :: String -> String
rStrip [] = []
rStrip xs = if isSpace (last xs)
            then rStrip (init xs)
            else xs

responseFor :: String -> String
responseFor xs
    | isSilence xs = "Fine. Be that way!"
    | last (rStrip xs) == '?' = if hasLetters xs && map toUpper xs == xs
                       then "Calm down, I know what I'm doing!"
                       else "Sure." 
    | hasLetters xs && map toUpper xs == xs = "Whoa, chill out!" 
    | otherwise = "Whatever."
