module ETL (transform) where

import Data.Map (Map, fromList, toList)
import Data.Char (toLower)

transform :: Map a String -> Map Char a
transform legacyData = fromList $ concat newData
    where
        newData = foldr (\x acc -> mapLegacy x:acc) [] $ toList legacyData

mapLegacy :: (a,String) -> [(Char,a)]
mapLegacy (key,xs) = foldl (\acc x -> (toLower x,key):acc) [] xs
